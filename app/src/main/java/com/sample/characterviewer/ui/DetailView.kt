package com.sample.characterviewer.ui

import android.os.Bundle
import android.view.MenuItem
import android.widget.ImageView
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import com.bumptech.glide.Glide
import com.sample.characterviewer.R


class DetailView : AppCompatActivity() {
    var detailImage: ImageView? = null
    var detailName: TextView? = null
    var detailDescription: TextView? = null
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_detail_view)
        setUI()
        setData()
    }

    private fun setData() {
        val intent = intent
        val name = intent.getStringExtra("name")
        val image = intent.getStringExtra("image")
        val description = intent.getStringExtra("result")
        detailName?.text = name
        detailDescription?.text = description
        val imageUrl = if (image?.isEmpty() == true) {
            R.drawable.image_duck
        } else {
            "https://duckduckgo.com$image"
        }
        Glide.with(this)
            .load(imageUrl)
            .into(detailImage!!)
    }

    private fun setUI() {
        detailImage = findViewById(R.id.image_view)
        detailName = findViewById(R.id.name_view)
        detailDescription = findViewById(R.id.detail_view)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)

    }


    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            android.R.id.home -> {
                // Handle the up button click
                finish()
                return true
            }
        }
        return super.onOptionsItemSelected(item)
    }


}
