package com.sample.characterviewer.ui

import android.os.Bundle
import android.view.View
import android.widget.ProgressBar
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.SearchView
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.sample.characterviewer.R
import com.sample.characterviewer.ui.adapters.CharacterAdapter
import com.sample.characterviewer.utils.Response
import com.sample.characterviewer.viewmodel.SimpsonsViewModel
import com.sample.simpsonsviewer.model.Characters
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class MainActivity : AppCompatActivity() {

    lateinit var recyclerView: RecyclerView
    lateinit var search: SearchView
     lateinit var dataViewModel: SimpsonsViewModel
    var load: ProgressBar? = null
    var error: TextView? = null
    var characterAdapter = CharacterAdapter()

    public override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        setUI()
        loadUI()

    }

    fun loadUI() {
        dataViewModel.sat.observe(this) {
            when (it) {
                is Response.Loading -> {
                    load?.visibility = View.VISIBLE
                    recyclerView.visibility = View.GONE
                    error?.visibility = View.GONE
                }

                is Response.Success -> {
                    load?.visibility = View.GONE
                    error?.visibility = View.GONE
                    recyclerView.visibility = View.VISIBLE
                    loadData(it)
                }

                is Response.Error -> {
                    error?.visibility = View.VISIBLE
                    recyclerView.visibility = View.GONE
                    load?.visibility = View.GONE
                }
            }


        }
    }

    private fun setUI() {
        recyclerView = findViewById(R.id.recyclerview_id)
        load = findViewById(R.id.load)
        error = findViewById(R.id.error_msg)
        search = findViewById(R.id.search)
        search.clearFocus()
        dataViewModel = ViewModelProvider(this)[SimpsonsViewModel::class.java]
        this.recyclerView.layoutManager = LinearLayoutManager(applicationContext)
        this.characterAdapter.also { this.recyclerView.adapter = it }
    }

    fun loadData(it: Response<Characters>) {
        var data = it.data?.RelatedTopics
        data?.sortedBy { it.Text }
        search.setOnQueryTextListener(object : SearchView.OnQueryTextListener {
            override fun onQueryTextSubmit(query: String?): Boolean {
                // Handle search query submission
                return true
            }

            override fun onQueryTextChange(newText: String?): Boolean {
                val filterData = data?.filter { it.Result.contains(newText.toString(), true) }
                if (filterData != null) {
                    characterAdapter.setData(filterData)
                }
                return true
            }
        })
        if (data != null) {
            characterAdapter.setData(data)
        }
    }
}






