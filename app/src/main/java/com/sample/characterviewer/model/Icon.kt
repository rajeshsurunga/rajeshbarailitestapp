package com.sample.simpsonsviewer.model

data class Icon(
    val Height: String,
    val URL: String,
    val Width: String
)