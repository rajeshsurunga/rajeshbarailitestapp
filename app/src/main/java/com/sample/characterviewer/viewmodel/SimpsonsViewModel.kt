package com.sample.characterviewer.viewmodel

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.sample.characterviewer.network.CharacterRepository
import com.sample.characterviewer.utils.Response
import com.sample.simpsonsviewer.model.Characters
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class SimpsonsViewModel @Inject constructor(private val repository: CharacterRepository) : ViewModel() {
    private val _sat = MutableLiveData<Response<Characters>>()
    val sat: LiveData<Response<Characters>>
        get() = _sat

    init {
        fetchSat()
    }

    fun fetchSat() {
        _sat.postValue((Response.Loading()))
        viewModelScope.launch {
            try {
               _sat.postValue((Response.Success(repository.getSat())))
            } catch (e: Exception) {
                _sat.postValue((e.message?.let { Response.Error(it) }))
            }
        }
    }

}