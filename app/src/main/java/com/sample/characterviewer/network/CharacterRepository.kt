package com.sample.characterviewer.network

import com.sample.simpsonsviewer.model.Characters
import javax.inject.Inject
import javax.inject.Named
import javax.inject.Singleton

@Singleton
class CharacterRepository @Inject constructor(
    private val apiService: ApiService, @Named("parameter_string") val params: String
) {

    suspend fun getSat(): Characters {
        return apiService.getSat(params)
    }
}


