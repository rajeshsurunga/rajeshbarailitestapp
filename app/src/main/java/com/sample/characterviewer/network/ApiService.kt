package com.sample.characterviewer.network

import com.sample.simpsonsviewer.model.Characters
import retrofit2.http.GET
import retrofit2.http.Query


interface ApiService {
        @GET("/")
        suspend fun getSat(
            @Query("q") query: String,
            @Query("format") format: String = "json"
        ): Characters
    }

