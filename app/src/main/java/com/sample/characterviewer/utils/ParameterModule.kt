package com.sample.characterviewer.utils

import com.sample.characterviewer.BuildConfig
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import javax.inject.Named
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
object ParameterModule {
    @Provides
    @Singleton
    @Named("parameter_string")
    fun provideMyString(): String {
        return BuildConfig.ACCESS_PARAMS
    }
}