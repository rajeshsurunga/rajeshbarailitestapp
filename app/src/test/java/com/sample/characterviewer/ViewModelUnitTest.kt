import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import com.sample.characterviewer.network.CharacterRepository
import com.sample.characterviewer.utils.Response
import com.sample.characterviewer.viewmodel.SimpsonsViewModel
import com.sample.simpsonsviewer.model.Characters
import com.sample.simpsonsviewer.model.Icon
import com.sample.simpsonsviewer.model.RelatedTopic
import io.mockk.coEvery
import io.mockk.mockk
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.test.TestCoroutineDispatcher
import kotlinx.coroutines.test.resetMain
import kotlinx.coroutines.test.setMain
import org.junit.After
import org.junit.Before
import org.junit.Rule
import org.junit.Test


class SatViewModelTest {
    @get:Rule
    val rule = InstantTaskExecutorRule()
    private val testDispatcher = TestCoroutineDispatcher()
    private lateinit var viewModel: SimpsonsViewModel
    private lateinit var repository: CharacterRepository

    @Before
    fun setup() {
        Dispatchers.setMain(testDispatcher)
        repository = mockk()
        viewModel = SimpsonsViewModel(repository)
    }

    @After
    fun cleanup() {
        Dispatchers.resetMain()
        testDispatcher.cleanupTestCoroutines()
    }

    @Test
    fun `test fetchSat with success response`() {
        val response = Characters(listOf(RelatedTopic("text", Icon("","akk","a"),"qww-ww","ww-ww")))
        coEvery { repository.getSat() } returns response

        //use the observer
        viewModel.sat.observeForever { }

        // Call ViewModelScope
        viewModel.fetchSat()

        // verifying the result
        assert(viewModel.sat.value is Response.Success)
        assert((viewModel.sat.value as Response.Success<Characters>).data == response)
        //removing the observer
        viewModel.sat.removeObserver {}
    }

    @Test
    fun `test fetchSat with error response`() {
        // mock Error Message when called getSat()
        val errorMsg = "error message"
        coEvery { repository.getSat() } throws Exception(errorMsg)

        //use the observer on livedata sat
        viewModel.sat.observeForever {}
        // Call ViewModelScope
        viewModel.fetchSat()

        // verifying the result
        assert(viewModel.sat.value is Response.Error)
        assert((viewModel.sat.value as Response.Error).errorMsg == errorMsg)

        //removing the observer
        viewModel.sat.removeObserver {}
    }
}